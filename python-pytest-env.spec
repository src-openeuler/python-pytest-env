%global _empty_manifest_terminate_build 0
Name:           python-pytest-env
Version:        1.1.4
Release:        1
Summary:        py.test plugin that allows you to add environment variables.
License:        MIT
URL:            https://github.com/pytest-dev/pytest-env
Source0:        https://files.pythonhosted.org/packages/b1/4d/5b11f903b1c253574b15168e13b32ea6482bb95f5964f090142b1cb75755/pytest_env-1.1.4.tar.gz
BuildArch:      noarch
%description
py.test plugin that allows you to add environment variables.

%package -n python3-pytest-env
Summary:        py.test plugin that allows you to add environment variables.
Provides:       python-pytest-env

# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-pytest
BuildRequires:  python3-hatchling
BuildRequires:  python3-hatch-vcs

%description -n python3-pytest-env
py.test plugin that allows you to add environment variables.

%package help
Summary:        py.test plugin that allows you to add environment variables.
Provides:       python3-pytest-env-doc
%description help
py.test plugin that allows you to add environment variables.

%prep
%autosetup -n pytest_env-%{version}

%build
%pyproject_build

%install
%pyproject_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pytest-env -f filelist.lst
%dir %{python3_sitelib}/*
%{python3_sitelib}/pytest_env/__pycache__/
%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Dec 10 2024 wanggang <wanggang1@kylinos.cn> - 1.1.4-1
- Update version to 1.1.4
- Bump actions/setup-python from 4 to 5 
- Bump pypa/gh-action-pypi-publish from 1.8.11 to 1.10.1

* Wed Jun 26 2024 liuzhilin <liuzhilin@kylinos.cn> - 1.1.3-1
- Upgrade package python3-pytest-env of version 1.1.3

* Wed Jul 5 2023 mengzhaoa <mengzhaoa@isoftstone.com> - 0.8.2-1
- Upgrade package python3-pytest-env of version 0.8.2

* Thu Jun 23 2022 OpenStack_SIG <openstack@openeuler.org> - 0.6.2-1
- Init package python3-pytest-env of version 0.6.2

